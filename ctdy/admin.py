from django.contrib import admin
from ctdy.models import GitInfo, BuildInfo,FtpInfo,RpmInfo,TaskInfo
# Register your models here.

admin.site.register(GitInfo)
admin.site.register(BuildInfo)
admin.site.register(FtpInfo)
admin.site.register(RpmInfo)
admin.site.register(TaskInfo)