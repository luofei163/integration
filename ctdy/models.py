from django.db import models

# Create your models here.
class FtpInfo(models.Model):
    ftp_ip = models.CharField(max_length=30)
    ftp_port = models.IntegerField();
    ftp_user = models.CharField(max_length=30)
    ftp_password = models.CharField(max_length=30) 
    
    def __str__(self):
        return self.ftp_ip
    
class GitInfo(models.Model):
    git_ip = models.CharField(max_length=30)
    git_port = models.IntegerField()
    git_user = models.CharField(max_length=30)
    
    def __str__(self):
        return self.git_ip
    
    
class RpmInfo(models.Model):
#     task_info = models.ForeignKey(TaskInfo)
    
    name = models.CharField(max_length=50)
    spec_name = models.CharField(max_length=200)
    patch_dir = models.CharField(max_length=50)
    source_dir = models.CharField(max_length=50)
    tarball_name = models.CharField(max_length=200)
    version = models.CharField(max_length=100)
    switch = models.BooleanField(default=False);
    sha1 = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name
    
    
class BuildInfo(models.Model):
    name = models.CharField(max_length=50)
    out_release = models.CharField(max_length=20)
    in_release = models.CharField(max_length=20)
    version = models.IntegerField()
    build_release_path = models.CharField(max_length=500)
    build_tmp_path = models.CharField(max_length=500)


    ftp_info = models.OneToOneField(FtpInfo)
    git_info = models.OneToOneField(GitInfo)
    
    rpm_info = models.ManyToManyField(RpmInfo)
    
    def __str__(self):
        return self.name

    
class TaskInfo(models.Model):
    task_id =models.AutoField(primary_key=True)  
    build_info = models.ForeignKey(BuildInfo)
    
    task_start_time = models.DateTimeField();
    task_end_time =  models.DateTimeField(null=True,blank=True);
    
    def __str__(self):
        return str(self.task_id)
    
    

    
    